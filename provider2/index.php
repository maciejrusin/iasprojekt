<?php
/*
 * provider i database settings
 */
$host = "localhost";
$database = "provider2";
$dbusername = "root";
$dbpassword = "";

$pdo = new PDO('mysql:host='.$host.';dbname='.$database.';charset=utf8mb4', $dbusername, $dbpassword);
$stmt = $pdo->query("SELECT * FROM wydawnictwo");
$results = $stmt->fetchAll(PDO::FETCH_ASSOC);


header('Content-Type: application/json');
echo json_encode($results);
