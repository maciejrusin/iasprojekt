<?php
// take data from providers
$provider1Response = file_get_contents('http://localhost/ias/provider1');
$provider2Response = file_get_contents('http://localhost/ias/provider2/');

// parse data from JSON to arrays
$ksiegarnia =  json_decode($provider1Response, true);
$wydawnictwo = json_decode($provider2Response, true);

// transform arrays with data and merge it
$items = array_merge(convertKsiegarnia($ksiegarnia), convertWydawnictwo($wydawnictwo));

// make some random order in response111
shuffle($items);

// show response in JSON
header('Content-Type: application/json');
echo json_encode($items);



function convertKsiegarnia($ksiegarnia){
    $result = [];
    foreach ($ksiegarnia as $pozycja){
        $item = [
            'provider_item_id' => $pozycja['id'],
            'tytul' => $pozycja['tytul'],
            'autorzy' => $pozycja['wykonawca'],
            'opis' => $pozycja['opis'],
            'strony' => $pozycja['strony'],
            'wydawca' => $pozycja['wydawca'],
            'wersja' => $pozycja['wersja'],
            'dodano' => $pozycja['dodano'],
            'typ' => 'pozycja'
        ];
        array_push($result, $item);
    }
    return $result;
}
function convertWydawnictwo($wydawnictwo){
    $result = [];
    foreach ($wydawnictwo as $baza){
        $item = [
            'provider_item_id' => $baza['id'],
            'tytul' => $baza['title'],
            'autorzy' => $baza['author'],
            'opis' => $baza['description'],
            'strony' => $baza['pages'],
            'wydawca' => $baza['publisher'],
            'wersja' => $baza['version'],
            'dodano' => $baza['added at'],
            'typ' => 'baza'
        ];
        array_push($result, $item);
    }
    return $result;
}